use ga2::Vector;
use num_traits::Zero;
use std::ops::{Add, AddAssign, Sub, SubAssign};

#[derive(Copy, Clone, Debug)]
pub struct Point<T> {
    pub x: T,
    pub y: T,
}

impl<T> Point<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: Zero> Default for Point<T> {
    fn default() -> Self {
        Self {
            x: T::zero(),
            y: T::zero(),
        }
    }
}

impl<T: Add<Output = T>> Add<Vector<T>> for Point<T> {
    type Output = Self;

    fn add(self, other: Vector<T>) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<T: Sub<Output = T>> Sub<Vector<T>> for Point<T> {
    type Output = Self;

    fn sub(self, other: Vector<T>) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<T: AddAssign> AddAssign<Vector<T>> for Point<T> {
    fn add_assign(&mut self, other: Vector<T>) {
        self.x += other.x;
        self.y += other.y;
    }
}

impl<T: SubAssign> SubAssign<Vector<T>> for Point<T> {
    fn sub_assign(&mut self, other: Vector<T>) {
        self.x -= other.x;
        self.y -= other.y;
    }
}

impl<T: Sub<Output = T>> Sub for Point<T> {
    type Output = Vector<T>;

    #[inline]
    fn sub(self, other: Self) -> Vector<T> {
        Vector {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

#[cfg(feature = "data-stream")]
mod data_stream;
