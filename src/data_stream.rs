use super::Point;

use data_stream::{FromStream, ToStream, from_stream, numbers::EndianSettings, to_stream};

use std::io::{Read, Result, Write};

impl<T: ToStream<S>, S: EndianSettings> ToStream<S> for Point<T> {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        to_stream::<S, _, _>(&self.x, stream)?;
        to_stream::<S, _, _>(&self.y, stream)
    }
}

impl<T: FromStream<S>, S: EndianSettings> FromStream<S> for Point<T> {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        Ok(Self {
            x: from_stream::<S, _, _>(stream)?,
            y: from_stream::<S, _, _>(stream)?,
        })
    }
}
